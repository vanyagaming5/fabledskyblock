package com.songoda.skyblock.bank;

public enum Type {
    WITHDRAW,
    DEPOSIT
}
