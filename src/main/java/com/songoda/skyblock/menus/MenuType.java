package com.songoda.skyblock.menus;

public enum MenuType {
    ADMIN_LEVELLING,
    ADMIN_CREATOR,
    ADMIN_GENERATOR,
    INFORMATION,
    COOP,
    LEVELLING,
    MEMBERS,
    BANS,
    VISIT,
    VISITORS
}
